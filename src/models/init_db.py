from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base


def init_db(connection_string):
    engine = create_engine(connection_string)
    base = declarative_base()
    base.metadata.create_all(engine)

    return engine

