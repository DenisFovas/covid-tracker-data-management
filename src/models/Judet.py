from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class Judet(Base):
    __tablename__ = 'judete'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return "<Judet(name='%s')>" % self.name
