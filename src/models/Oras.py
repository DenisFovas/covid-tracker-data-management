from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import declarative_base, relationship

from models import Judet

Base = declarative_base()


class Oras(Base):
    __tablename__ = 'orase'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    judet_id = Column(Integer, ForeignKey('judet.id'))

    oras = relationship('Oras', back_populates='orase')

    def __repr__(self):
        return "<Oras(name='%s', judet='%s')>" % (self.name, self.judet)


Judet.orase = relationship("Oras", order_by=Oras.name, back_populates="judet")
