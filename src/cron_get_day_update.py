#!python3

from sqlalchemy import create_engine
from datetime import date, timedelta
import pandas as pd
from bs4 import BeautifulSoup
import requests
import os
from dotenv import load_dotenv

from .database import to_sql

base_url = 'https://data.gov.ro/dataset/transparenta-covid'


def get_download_link_for_latest_update():
    page = requests.get(base_url)
    soup = BeautifulSoup(page.content, "html.parser")
    section = soup.find(id='dataset-resources')
    li_tags = section.find_all('li')
    last_element_in_list = li_tags[-1]
    a = last_element_in_list.extract()
    link = a.findChild('a')
    return link['href']


def write_new_file(download_link):
    yesterday = date.today() - timedelta(days=1)
    FILE_PATH = f'./data/{str(yesterday)}.csv'

    r = requests.get(download_link, allow_redirects=True)
    open('latest_download.xlsx', 'wb').write(r.content)
    latest_download = pd.read_excel('latest_download.xlsx', header=2)

    new_data = latest_download[['UAT', str(yesterday)]]
    new_data.to_csv(FILE_PATH, index=False)

    return FILE_PATH


def download_latest_update():
    download_link = get_download_link_for_latest_update()
    file = write_new_file(download_link)

    return file


def insert_new_days(file):
    engine = create_engine(os.environ['connection'])
    df = pd.read_csv(file)
    to_sql(engine, df, 'incidenta', create_table=False)


if __name__ == '__main__':
    load_dotenv('./../.env')
    file = download_latest_update()
    insert_new_days(file)
