Bottleneck==1.3.2
certifi==2021.10.8
cycler @ file:///tmp/build/80754af9/cycler_1637851556182/work
et-xmlfile==1.1.0
fonttools==4.25.0
greenlet @ file:///opt/concourse/worker/volumes/live/95ce977d-8383-4e59-44f1-32af4c61496e/volume/greenlet_1628888142951/work
kiwisolver @ file:///opt/concourse/worker/volumes/live/0b2f3e77-eaa3-4995-7dd0-c994762fcbde/volume/kiwisolver_1612282417472/work
matplotlib @ file:///opt/concourse/worker/volumes/live/4863dc73-443d-42f7-68bf-489aaa0e23a1/volume/matplotlib-suite_1638289695940/work
mkl-fft==1.3.1
mkl-random @ file:///opt/concourse/worker/volumes/live/f196a661-8e33-4aac-63bc-efb2ff50e035/volume/mkl_random_1626186080069/work
mkl-service==2.4.0
munkres==1.1.4
numexpr @ file:///opt/concourse/worker/volumes/live/e845d683-bbb9-4fa2-79ce-743b84c61560/volume/numexpr_1618856522192/work
numpy @ file:///opt/concourse/worker/volumes/live/ceac9fd4-1ae1-48f8-612a-0c99bf783d0e/volume/numpy_and_numpy_base_1634106709647/work
olefile @ file:///Users/ktietz/demo/mc3/conda-bld/olefile_1629805411829/work
openpyxl @ file:///tmp/build/80754af9/openpyxl_1632777717936/work
packaging @ file:///tmp/build/80754af9/packaging_1637314298585/work
pandas==1.3.4
Pillow==8.4.0
pyparsing @ file:///tmp/build/80754af9/pyparsing_1635766073266/work
python-dateutil @ file:///tmp/build/80754af9/python-dateutil_1626374649649/work
python-dotenv==0.19.2
pytz==2021.3
six @ file:///tmp/build/80754af9/six_1623709665295/work
SQLAlchemy @ file:///opt/concourse/worker/volumes/live/488db394-eaa7-4733-4ad9-8b68b58bb1a0/volume/sqlalchemy_1638290685477/work
tornado @ file:///opt/concourse/worker/volumes/live/05341796-4198-4ded-4a9a-332fde3cdfd1/volume/tornado_1606942323372/work
tqdm @ file:///tmp/build/80754af9/tqdm_1635330843403/work

numpy~=1.21.2
matplotlib~=3.5.0
tqdm~=4.62.3
sqlalchemy~=1.4.27