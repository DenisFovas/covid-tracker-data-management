import pandas as pd
import os
import logging

from dotenv import load_dotenv
from tqdm import tqdm

from models import init_db


def init_logger():
    logging.basicConfig(filename=f'logs/log.log',
                        format='%(asctime)s %(levelname)s:%(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.DEBUG,
                        filemode='a')

    return logging.getLogger()


def read_data():
    logging.info('Reading the file datas.')
    dfs = []
    for files in (os.walk('./data')):
        for file in tqdm(files[2]):
            logging.info('reading file = {}'.format(file))
            a = pd.read_excel(os.path.join('data', file), header=2)
            dfs.append(a)

    data_frame = pd.concat(dfs)
    data_frame = data_frame.melt(data_frame.columns[:2])
    data_frame = data_frame.rename(columns={'variable': 'Date', 'value': 'Indice'})

    return data_frame

if __name__ == '__main__':
    load_dotenv('.env')  # take environment variables from .env.

    engine = init_db(os.environ['connection'])
    logger = init_logger()
    # df = read_data()


